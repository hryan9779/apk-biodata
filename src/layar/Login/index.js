import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React from 'react'

const Login = ({navigation}) => {
  return (
    <View style={styles.body}>
      <Text style={{fontSize: 40, color: 'black',}}>LOGIN</Text>
      <TextInput placeholder='Username' style={{borderWidth: 1, width: 300, borderRadius: 10, marginTop: 10,}}/>
      <TextInput placeholder='Password' keyboardType='visible-password' style={{borderWidth: 1, width: 300, borderRadius: 10, marginTop: 10,}}/>
      <TouchableOpacity style={{backgroundColor: 'blue', width: 100, height: 50, marginTop: 40, borderRadius: 10, justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{color: 'white', fontSize: 20,}} onPress={() => navigation.navigate("Home")}>Masuk</Text>
      </TouchableOpacity>
    </View>
  )
}

export default Login

const styles = StyleSheet.create({
    body: {
        backgroundColor: 'white',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    }
})