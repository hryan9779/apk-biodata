import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { IconAlamat, IconJurusan, IconNIM, IconUser, Imageuser } from '../../assets'

const Home = () => {
  return (
    <View>
      <View style={{backgroundColor: 'orange', width: '100%', height: 300, alignItems: 'center', justifyContent: 'center'}}>
        <View >
            <Image source={Imageuser} style={{backgroundColor: 'white', width: 100, height: 100, borderRadius: 200,}}/>
        </View>
        <Text style={{marginTop: 15, fontSize: 25, color: 'black', fontWeight: 'bold'}}>
            Rian Hidayatullah
        </Text>
        <Text>rianhidayat@gmail.com</Text>
      </View>
      <View style={{padding: 50}}>
        <Text style={{fontSize: 30, color: 'black', fontWeight: 'bold'}}>Biodata</Text>
        <View>
            <View style={{marginTop: 20, display: 'flex', flexDirection: 'row'}}>
                <View style={{marginRight: 10, justifyContent: 'center'}}>
                    <IconUser />
                </View>
                <View>
                    <Text style={{fontWeight: 'bold', color:'black'}}>
                        Nama Lengkap
                    </Text>
                    <Text style={{color: 'black', fontSize: 22,}}>Rian Hidayatullah</Text>
                </View>
            </View>
            <View style={{marginTop: 20, display: 'flex', flexDirection: 'row'}}>
                <View style={{marginRight: 10, justifyContent: 'center'}}>
                    <IconAlamat />
                </View>
                <View>
                    <Text style={{fontWeight: 'bold', color:'black'}}>
                        Alamat
                    </Text>
                    <Text style={{color: 'black', fontSize: 22,}}>Desa Palengaan</Text>
                </View>
            </View>
            <View style={{marginTop: 20, display: 'flex', flexDirection: 'row'}}>
                <View style={{marginRight: 10, justifyContent: 'center'}}>
                    <IconJurusan />
                </View>
                <View>
                    <Text style={{fontWeight: 'bold', color:'black'}}>
                        Jurusan
                    </Text>
                    <Text style={{color: 'black', fontSize: 22,}}>Teknik Informatika</Text>
                </View>
            </View>
            <View style={{marginTop: 20, display: 'flex', flexDirection: 'row'}}>
                <View style={{marginRight: 10, justifyContent: 'center'}}>
                    <IconNIM />
                </View>
                <View>
                    <Text style={{fontWeight: 'bold', color:'black'}}>
                        Nomer Induk Mahasiswa
                    </Text>
                    <Text style={{color: 'black', fontSize: 22,}}>2020020100008</Text>
                </View>
            </View>
        </View>
      </View>
    </View>
  )
}

export default Home

const styles = StyleSheet.create({})