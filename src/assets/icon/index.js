import IconUser from './user.svg';
import IconAlamat from './Alamat.svg';
import IconJurusan from './Jurusan.svg';
import IconNIM from './NIM.svg';

export { IconUser, IconAlamat, IconJurusan, IconNIM}